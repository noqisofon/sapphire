# -*- coding: utf-8;
#+title: コモンモジュールのヘッダー
#+language: ja

#+begin_src c :tangle ./str.h :exports none
  #pragma once
#+end_src

C++ だと ~extern "C"~ みたいなのが必要です。

#+begin_src c :tangle ./str.h
  #ifdef __cplusplus
  #   define SPH_BEGIN_EXTERN_C    extern "C" {
  #   define SPH_END_EXTERN_C      }
  #else
  #   define SPH_BEGIN_EXTERN_C    
  #   define SPH_END_EXTERN_C      
  #endif
#+end_src

MSVC においては ~inline~ は ~__inline~ になります(多分)。

#+begin_src c :tangle ./str.h
  #ifndef defined _MSC_VER
  #   define SPH_INLINE            static __inline
  #else
  #   define SPH_INLINE            static   inline
  #endif   /* ndef defined _MSC_VER */
#+end_src

~SPH_PLATFORM_IS_WINDOWS~ は ~msys2~ 環境や ~mingw32~ 環境で定義されていると思います(おそらく)。

#+begin_src c :tangle ./str.h
  #ifdef SPH_PLATFORM_IS_WINDOWS
  #   if defined(SPH_BUILD) && defined(SPH_DLL_BUILD)
  #       define SPH_DECLSPEC      __declspec(dllimport)
  #   else
  #       define SPH_DECLSPEC      __declspec(dllexport)
  #   endif  /* defined(SPH_BUILD) && defined(SPH_DLL_BUILD) */
  #else
  #   define SPH_DECLSPEC          extern
  #endif  /* def SPH_PLATFORM_IS_WINDOWS */
#+end_src

外に公開する関数に ~SPH_API~ を付けるようにします。

#+begin_src c :tangle ./str.h
  #define SPH_API                  SPH_DECLSPEC
#+end_src


#+begin_src c :tangle ./str.h :exports none
  SPH_BEGIN_EXTERN_C
#+end_src

#+begin_src c :tangle ./str.h :exports none
  SPH_END_EXTERN_C
#+end_src
