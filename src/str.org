# -*- coding: utf-8; -*-
#+title: 文字列モジュールの実装
#+language: ja


#+begin_src c :tangle ./str.c :exports none
  #include "sapphire/str.h"
#+end_src

* ~SPH_str_chop()~

~SPH_str_chop()~ は文字列の末尾を 1 文字削った後の文字列を返します。

#+begin_src c :tangle ./str.c
  SPH_API SPHStr SPH_str_chop(SPHStr self) {
      SPHsize_t size   = SPH_str_bytes( self );
      SPHStr    result = SPH_str_new( size - 1 );

    

      return result;
  }
#+end_src
